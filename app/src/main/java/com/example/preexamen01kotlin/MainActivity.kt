package com.example.preexamen01kotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var lblNombreTrabajador: TextView
    private lateinit var txtNombreTrabajador: EditText

    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()

        //Eventos click de los botones
        btnEntrar.setOnClickListener { entrar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador)
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun entrar() {
        var strNombre: String
        strNombre = applicationContext.resources.getString(R.string.nombre)
        if (strNombre == txtNombreTrabajador.text.toString().trim { it <= ' ' }) {
            // Hacer el paquete para enviar la información
            var bundle = Bundle()
            bundle.putString("nombre", txtNombreTrabajador.text.toString())

            // Crear el Intent para llamar a otra actividad
            var intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando respuesta o no
            startActivity(intent)
        } else {
            Toast.makeText(
                this.applicationContext, "El nombre de usuario no es válido",
                Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir() {
        finish()
    }
}