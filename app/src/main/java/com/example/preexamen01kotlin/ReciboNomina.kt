package com.example.preexamen01kotlin

class ReciboNomina {
    // Declaración de varibles privadas en la clase
    private var numRecibo = 0
    private var nombre: String? = null
    private var horasTrabNormal = 0f
    private var horasTrabExtras = 0f
    private var puesto = 0
    private var impuestoPorc = 0f

    // Constructor de parámetros para variables públicas
    fun ReciboNomina(numRecibo: Int, nombre: String?, horasTrabNormal: Float, horasTrabExtras: Float,
        puesto: Int, impuestoPorc: Float) {
        this.numRecibo = numRecibo
        this.nombre = nombre
        this.horasTrabNormal = horasTrabNormal
        this.horasTrabExtras = horasTrabExtras
        this.puesto = puesto
        this.impuestoPorc = impuestoPorc
    }

    // Set & Get de cada variable
    fun setNumRecibo(numRecibo: Int) {
        this.numRecibo = numRecibo
    }

    fun getNumRecibo(): Int {
        return numRecibo
    }

    fun setNombre(nombre: String?) {
        this.nombre = nombre
    }

    fun getNombre(): String? {
        return nombre
    }

    fun setHorasTrabNormal(horasTrabNormal: Float) {
        this.horasTrabNormal = horasTrabNormal
    }

    fun getHorasTrabNormal(): Float {
        return horasTrabNormal
    }

    fun setHorasTrabExtras(horasTrabExtras: Float) {
        this.horasTrabExtras = horasTrabExtras
    }

    fun getHorasTrabExtras(): Float {
        return horasTrabExtras
    }

    fun setPuesto(puesto: Int) {
        this.puesto = puesto
    }

    fun getPuesto(): Int {
        return puesto
    }

    fun setImpuestoPorc(impuestoPorc: Float) {
        this.impuestoPorc = impuestoPorc
    }

    fun getImpuestoPorc(): Float {
        return impuestoPorc
    }

    // Cálculo subtotal
    fun calcularSubtotal(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        var pagoBase = 200f
        var subtotal = 0f
        // Cálculo de pago base según el puesto
        if (puesto == 1) {
            pagoBase = (pagoBase + pagoBase * 0.20).toFloat()
        } else if (puesto == 2) {
            pagoBase = (pagoBase + pagoBase * 0.50).toFloat()
        } else if (puesto == 3) {
            pagoBase = (pagoBase + pagoBase * 1.00).toFloat()
        } else {
            println("Calculo no valido")
        }
        // Cálculo del subtotal (se paga al doble)
        subtotal = pagoBase * horasTrabNormal + horasTrabExtras * pagoBase * 2
        return subtotal
    }

    // Cálculo impuesto
    fun calcularImpuesto(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        return (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16).toFloat()
    }

    // Cálculo total
    fun calcularTotal(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        return calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) - calcularImpuesto(puesto,
            horasTrabNormal, horasTrabExtras)
    }
}