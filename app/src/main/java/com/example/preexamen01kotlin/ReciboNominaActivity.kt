package com.example.preexamen01kotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    // Declaración de objetos
    private lateinit var lblUsuario: TextView
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasTrabNormales: EditText
    private lateinit var txtHorasTrabExtras: EditText
    private lateinit var rbAuxiliar: RadioButton
    private lateinit var rbAlbañil: RadioButton
    private lateinit var rbIngObra: RadioButton

    private lateinit var txtSubtotal: EditText
    private lateinit var txtImpuesto: EditText
    private lateinit var txtTotalPagar: EditText

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private var recibo = ReciboNomina()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo)
        iniciarComponentes()

        // Obtener los datos del MainActivity

        // Obtener los datos del MainActivity
        var datos = intent.extras
        var nombre = datos!!.getString("nombre")
        lblUsuario.text = nombre

        // Evento click de los botones
        btnCalcular.setOnClickListener { calcular() }
        btnLimpiar.setOnClickListener { limpiar() }
        btnRegresar.setOnClickListener { regresar() }
    }

    private fun iniciarComponentes() {
        // Etiqueta
        lblUsuario = findViewById(R.id.lblUsuario)

        // Cajas de texto
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasTrabNormales = findViewById(R.id.txtHorasTrabNormales)
        txtHorasTrabExtras = findViewById(R.id.txtHorasTrabExtras)
        txtSubtotal = findViewById(R.id.txtSubtotal)
        txtImpuesto = findViewById(R.id.txtImpuesto)
        txtTotalPagar = findViewById(R.id.txtTotalPagar)

        // Radio Buttons
        rbAuxiliar = findViewById(R.id.rbAuxiliar)
        rbAlbañil = findViewById(R.id.rbAlbañil)
        rbIngObra = findViewById(R.id.rbIngObra)

        // Botones
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        // Objeto ReciboNomina inicializado
        recibo = ReciboNomina()

        deshabilitarTextos()
    }

    private fun calcular() {
        var puesto = 0
        var horasNormales = 0f
        var horasExtras = 0f
        var subtotal = 0f
        var impuesto = 0f
        var total = 0f

        if (rbAuxiliar.isChecked) {
            puesto = 1
        } else if (rbAlbañil.isChecked) {
            puesto = 2
        } else if (rbIngObra.isChecked) {
            puesto = 3
        } else if (puesto == 0) {
            Toast.makeText(this, "Por favor, seleccione un puesto", Toast.LENGTH_SHORT).show()
        }


        if (txtNumRecibo.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese un número de recibo",
                Toast.LENGTH_SHORT).show()
        } else if (txtNombre.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese un nombre", Toast.LENGTH_SHORT).show()
        } else if (txtHorasTrabNormales.text.toString().isEmpty() || txtHorasTrabExtras.text.toString().isEmpty()) {
            if (txtHorasTrabNormales.text.toString().isEmpty()) {
                Toast.makeText(this, "Ingrese una cantidad de horas trabajadas",
                    Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Ingrese una cantidad de horas extras",
                    Toast.LENGTH_SHORT).show()
            }
        }
        if (!txtHorasTrabNormales.text.toString().isEmpty() && !txtHorasTrabExtras.text.toString().isEmpty() &&
            puesto != 0) {

            horasNormales = txtHorasTrabNormales.text.toString().toFloat()
            horasExtras = txtHorasTrabExtras.text.toString().toFloat()
            subtotal = recibo.calcularSubtotal(puesto, horasNormales, horasExtras)
            impuesto = recibo.calcularImpuesto(puesto, horasNormales, horasExtras)
            total = recibo.calcularTotal(puesto, horasNormales, horasExtras)

            // Mostrar el resultado de los cálculos
            txtSubtotal.setText("" + subtotal)
            txtImpuesto.setText("" + impuesto)
            txtTotalPagar.setText("" + total)
        }
    }

    private fun limpiar() {
        txtNumRecibo.setText("")
        txtNombre.setText("")
        txtHorasTrabNormales.setText("")
        txtHorasTrabExtras.setText("")
        txtSubtotal.setText("")
        txtImpuesto.setText("")
        txtTotalPagar.setText("")
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo de Nómina")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->  }
        confirmar.show()
    }

    private fun deshabilitarTextos() {
        txtSubtotal.isFocusable = false
        txtImpuesto.isFocusable = false
        txtTotalPagar.isFocusable = false
    }
}